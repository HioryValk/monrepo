from PySide2 import QtWidgets, QtGui, QtCore
import os
import json
import importlib

from lexic import get_liste_classe, DATA_DIR, get_liste_composes, get_liste_ne, files
import lexic

class App_analyse(QtWidgets.QWidget):
    def __init__(self, phrase):
        super().__init__()
        self.phrase = phrase
        self.setMinimumSize(700, 70)
        self.setMaximumSize(2000, 70)

        self.setup_ui()

    def setup_ui(self):
        self.boite = QtWidgets.QHBoxLayout(self)
        self.text_frame = QtWidgets.QTextBrowser()
        self.text_frame.setText(self.phrase)
        self.text_frame.setAlignment(QtCore.Qt.AlignCenter)

        self.boite.addWidget(self.text_frame)

class App_error(QtWidgets.QErrorMessage):
    def __init__(self, message):
        super().__init__()
        self.showMessage(f"{message}")


class App_liste(QtWidgets.QWidget):

    def __init__(self, liste):
        super().__init__()
        self.setup_ui()
        self.setup_connections()
        self.liste = liste
        self.populate()

    def populate(self):
        self.liste_box.setText(f"{self.liste}")

    def setup_ui(self):
        self.boite = QtWidgets.QVBoxLayout(self)
        self.liste_box = QtWidgets.QMessageBox()
        self.liste_box.setFixedSize(100, 50)

        self.add(self.liste_box)

    def setup_connections(self):
        self.liste_box.buttonClicked.connect(self.closer)

    def closer(self):
        self.close()

    def add(self, element):
        self.boite.addWidget(element)


class App(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()
        self.setup_ui()
        self.setWindowTitle("mapseba")
        self.set_default_values()
        self.setup_connections()
        self.setFixedSize(1150, 600)

    def setup_ui(self):
        self.boite = QtWidgets.QHBoxLayout(self)

        self.boite_left = QtWidgets.QVBoxLayout()
        self.boite_left_in = QtWidgets.QHBoxLayout()
        self.le_mot = QtWidgets.QLineEdit()
        self.enter_btn = QtWidgets.QPushButton()
        self.enter_btn.setIcon(QtGui.QIcon('images.png'))
        self.lw_result = QtWidgets.QListWidget()
        self.boite_left_in2 = QtWidgets.QHBoxLayout()
        self.boite_left_in2.setAlignment(QtCore.Qt.AlignLeft)
        self.red_frame = QtWidgets.QFrame()
        self.red_frame.setFixedSize(20, 23)
        self.red_frame.setStyleSheet("background-color : rgb(100,200,0)")
        self.text_verbe = QtWidgets.QTextBrowser()
        self.text_verbe.setText("VERBE")
        self.text_verbe.setFixedSize(70, 23)
        self.text_verbe.setStyleSheet("border : none")
        self.blue_frame = QtWidgets.QFrame()
        self.blue_frame.setFixedSize(20, 23)
        self.blue_frame.setStyleSheet("background-color : rgb(0,0,255)")
        self.text_sujet = QtWidgets.QTextBrowser()
        self.text_sujet.setFixedSize(70, 23)
        self.text_sujet.setText("SUJET")
        self.text_sujet.setStyleSheet("border : none")

        self.boite_right = QtWidgets.QVBoxLayout()
        self.combo = QtWidgets.QComboBox()
        self.boite_right_in = QtWidgets.QVBoxLayout()
        self.checkbox_classe = QtWidgets.QCheckBox()
        self.checkbox_nominal = QtWidgets.QCheckBox()
        self.checkbox_verbal = QtWidgets.QCheckBox()
        self.checkbox_classe.setText("classe des mots")
        self.checkbox_nominal.setText("groupe nominaux")
        self.checkbox_verbal.setText("groupe verbaux")
        self.classe_btn = QtWidgets.QCommandLinkButton("ajouter/supprimer classe")
        self.mot_btn = QtWidgets.QCommandLinkButton("ajouter/supprimer mot")
        self.regle_btn = QtWidgets.QCommandLinkButton("ajouter/supprimer regle")
        self.display_btn = QtWidgets.QCommandLinkButton("afficher la liste")

        self.boite_left_in.addWidget(self.le_mot)
        self.boite_left_in.addWidget(self.enter_btn)

        self.boite_left_in2.addWidget(self.red_frame)
        self.boite_left_in2.addWidget(self.text_verbe)
        self.boite_left_in2.addWidget(self.blue_frame)
        self.boite_left_in2.addWidget(self.text_sujet)

        self.boite_left.addLayout(self.boite_left_in)
        self.boite_left.addWidget(self.lw_result)
        self.boite_left.addLayout(self.boite_left_in2)

        self.boite_right_in.addWidget(self.checkbox_classe)
        self.boite_right_in.addWidget(self.checkbox_nominal)
        self.boite_right_in.addWidget(self.checkbox_verbal)
        self.boite_right_in.addWidget(self.combo)

        self.boite_right.addLayout(self.boite_right_in)
        self.boite_right.addWidget(self.classe_btn)
        self.boite_right.addWidget(self.mot_btn)
        self.boite_right.addWidget(self.regle_btn)
        self.boite_right.addWidget(self.display_btn)


        self.boite.addLayout(self.boite_left)
        self.boite.addLayout(self.boite_right)

    def set_default_values(self):
        self.combo.addItems(sorted(get_liste_classe()))

    def setup_connections(self):
        self.mot_btn.clicked.connect(self.addtolist)
        self.display_btn.clicked.connect(self.display_list)
        self.enter_btn.clicked.connect(self.analyser)
        self.le_mot.returnPressed.connect(self.analyser)
        self.classe_btn.clicked.connect(self.add_classe)

    def analyser(self):
        if len(self.le_mot.text()) > 0:
            result = self.get_type()
            if result:
                self.win_analyse = App_analyse(f"{result}")
                self.win_analyse.show()
        else:
            self.win_analyse = App_error("Aucune phrase à analyser ...")
            self.win_analyse.show()



    def add_classe(self):
        mot = self.le_mot.text()
        NEW_LIST_DIR = os.path.join(DATA_DIR, f"{mot}.json")
        if " " in mot:
            self.win_error = App_error("Remplacez les espaces par '_'")
            self.win_error.show()
        elif len(mot) <= 0:
            self.win_error = App_error("Aucun nom de classe")
            self.win_error.show()
        else:
            with open(NEW_LIST_DIR, 'w', encoding='utf8') as f:
                json.dump([], f, ensure_ascii=False)
            importlib.reload(lexic)
            liste = sorted(get_liste_classe())
            self.combo.clear()
            self.combo.addItems(liste)


    def addtolist(self):
        mot = self.le_mot.text()
        list_name = self.combo.currentText()
        TARGET_DIR = os.path.join(DATA_DIR, f"{list_name}.json")
        if len(mot) > 0:
            with open(TARGET_DIR, 'r', encoding='utf8') as f:
                liste = json.load(f)
            liste.append(mot)
            with open(TARGET_DIR, 'w', encoding='utf8') as f:
                json.dump(liste, f, ensure_ascii=False, indent=4)
        else:
            self.win_error = App_error("Aucun mot à ajouter/supprimer")
            self.win_error.show()

    def display_list(self):
        list_name = self.combo.currentText()
        TARGET_DIR = os.path.join(DATA_DIR, f"{list_name}.json")
        with open(TARGET_DIR, 'r', encoding='utf8') as f:
            liste = json.load(f)
        self.dialog = App_liste(liste)
        self.dialog.show()

    def get_phrase(self):
        phrase = self.le_mot.text()
        liste_mot = []
        if phrase:
            phrase = phrase.split(" ")
            for mot in phrase:
                liste_mot.append(mot)
            if liste_mot:
                return liste_mot
        else:
            return False

    def get_type(self):
        d = {}
        liste_mot = self.get_phrase()
        for mot in liste_mot:
            if mot == 'ne':
                for i in get_liste_ne():
                    if i in liste_mot:
                        for element in get_liste_composes():
                            if i in element:
                                mot = element
            set_mot = set()
            for file in files:
                if file.endswith(".json"):
                    with open(file, "r", encoding="utf8") as f:
                        json_file = json.load(f)
                        if mot in json_file:

                            set_mot.add(os.path.basename(file).split(".")[0])
                if not set_mot:
                    d[mot] = "inconnu"
                else:
                    d[mot] = set_mot
        for mot in get_liste_composes():
            if mot in d:
                for composant in get_liste_ne():
                    if composant in mot:
                        del d[composant]

        return d

    def test(self):
        print(self.combo.currentText())


app = QtWidgets.QApplication()
win = App()
win.show()
app.exec_()