from PySide2 import QtWidgets, QtGui

class App_liste(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()
        self.setup_ui()
        self.setup_connections()

    def setup_ui(self):
        self.boite = QtWidgets.QVBoxLayout(self)
        self.liste_box = QtWidgets.QMessageBox()

        self.add(self.liste_box)

    def setup_connections(self):
        self.liste_box.buttonClicked.connect(self.closer)

    def closer(self):
        App_liste.close(self)

    def add(self, element):
        self.boite.addWidget(element)






