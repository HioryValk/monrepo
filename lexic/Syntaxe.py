import sqlite3
import os
import glob
import json

# poids potentiel proposition principale
Proposition = 0
GroupeNominal = 0
Sujet = 0
Verbe=0

# Poids obligatoire
Pronom_Personel = False
Preposition = False
Determinant = False
Adverbe_negation = False
Pronom = False
Si = False

                           # liste
list_pp = ['je', 'tu', 'il']
list_prep = ["à", "dans", "sur", "avec"]
list_det = ["de", "le", "la", "un"]
list_advn = ["ne"]
list_pro = ["mon", "ma", "mes"]
list_si = ["si"]
list_verb = ["suis", "mange", "est"]
list_nom = ["ballon", "chat", "sadoun"]
                         # Entrer une phrase
Synt = False
while Synt == False:
    Phrase = input("vous: ")
    if Phrase == " " or Phrase == "":
        print("Syntaxe Incorrecte")
        Synt = False
    else:
        Synt = True
# Analyser une phrase
Phrase_dec = Phrase
Phrase_dec = Phrase_dec.split(" ")
print(Phrase_dec)
list_groupe = []
groupes = 0
for mot in Phrase_dec:
    print(mot)
    if mot in list_pp:
        if Preposition == True:
            Preposition = False
        elif Si == True:
            Si = False
        Pronom_Personel = True

    elif mot in list_prep:
        Preposition = True

    elif mot in list_det:
        Determinant = True

    elif mot in list_advn:
        if Adverbe_negation == True:
            Adverbe_negation = False
        Adverbe_negation = True

    elif mot in list_pro:
        if Si == True:
            Si = False
        Pronom_Personel = True

    elif mot in list_si:
        Si = True

    elif mot in list_verb:
        if Pronom_Personel == True:
            Pronom_Personel = False

    elif mot in list_nom:
        if Pronom == True:
            Pronom = False
        elif Determinant == True:
            Determinant = False
        elif Preposition == True:
            Preposition = False

    if Pronom_Personel == False and Preposition == False and Determinant == False and Adverbe_negation == False and Pronom == False and Si == False:
        print("Groupe ajouté :")
        groupes = groupes + 1
        nom_groupe = "groupe " + str(groupes)
        list_groupe.append(nom_groupe)


print(list_groupe)

def Recherche_mot (mot):
