from PySide2 import QtWidgets, QtCore, QtGui
import os
import json

from lexic import WORD_DIR, files, get_liste_composes, get_liste_ne

class App(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Lexique")
        self.setup_ui()
        self.setup_connections()
        self.counter = 0

    def setup_ui(self):
        self.boite = QtWidgets.QVBoxLayout(self)
        self.le_text = QtWidgets.QLineEdit()
        self.btn_ajouter = QtWidgets.QPushButton("Ajouter à la liste")
        self.lw = QtWidgets.QListWidget()
        self.lw.setSelectionMode(QtWidgets.QListWidget.ExtendedSelection)
        self.btn_analyser = QtWidgets.QPushButton("Analyser")

        self.boite.addWidget(self.le_text)
        self.boite.addWidget(self.btn_ajouter)
        self.boite.addWidget(self.lw)
        self.boite.addWidget(self.btn_analyser)

    def setup_connections(self):
        self.btn_ajouter.clicked.connect(self.ajouter)
        self.le_text.returnPressed.connect(self.ajouter)
        self.btn_analyser.clicked.connect(self.analyser)


    def ajouter(self):
        import random

        texte = self.le_text.text()
        a = random.randint(0, 256)
        b = random.randint(0, 256)
        c = random.randint(0, 256)
        if texte:
            self.lw.addItem(texte)
            items = self.lw.findItems(self.lw.item(self.counter).text(), QtCore.Qt.MatchExactly)
            for item in items:
                item.setTextColor(QtGui.QColor.fromRgb(a, b, c))
            self.le_text.clear()
            self.counter += 1

    def analyser(self):
        if len(self.lw.selectedItems()) == 1:
            if self.get_type():
                print(self.get_type())
            else:
                print("nom")

        elif len(self.lw.selectedItems()) < 1:
            print("séléctionnez un item svp ...")
        else:
            print("trop de phrases séléctionées en meme temps")

    def get_phrase(self):
        liste_mot = []
        if self.lw.currentItem():
            phrase = self.lw.currentItem().text().split(" ")
            for mot in phrase:
                liste_mot.append(mot)
            if liste_mot:
                return liste_mot
        else:
            return False

    def get_type(self):
        d = {}
        liste_mot = self.get_phrase()
        for mot in liste_mot:
            if mot == 'ne':
                for i in get_liste_ne():
                    if i in liste_mot:
                        for element in get_liste_composes():
                            if i in element:
                                mot = element
            set_mot = set()
            for file in files:
                if file.endswith(".json"):
                    with open(file, "r", encoding="utf8") as f:
                        json_file = json.load(f)
                        if mot in json_file:

                            set_mot.add(os.path.basename(file).split(".")[0])
                if not set_mot:
                    d[mot] = "inconnu"
                else:
                    d[mot] = set_mot
        for mot in get_liste_composes():
            if mot in d:
                for composant in get_liste_ne():
                    if composant in mot:
                        del d[composant]

        return d



app = QtWidgets.QApplication()
win = App()
win.show()
app.exec_()

