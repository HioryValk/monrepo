import os
import glob
import json

CURR_DIR = os.path.abspath(os.path.dirname(__file__))
DATA_DIR = os.path.join(CURR_DIR, "data")
WORD_DIR = os.path.join(DATA_DIR, "LEs Mots")
Adverb_dir = os.path.join(WORD_DIR, "adverbe")
files = glob.glob(os.path.join(DATA_DIR, "**"), recursive=True)

def get_type_mot(mot):
    final_liste = []
    for file in files:
        if file.endswith(".json"):
            classe = os.path.basename(file).split(".")[0]
            with open(file, 'r', encoding='utf8') as f:
                liste = json.load(f)
            if mot in liste:
                final_liste.append(classe)
    return final_liste

def principale(conteur, phrase):
    groupe = []
    conteur_indice = 0
    for mot in phrase:
        classe = get_type_mot(mot)
        if classe:
            classe = classe[0]
        if conteur == conteur_indice:
            groupe.append(mot)
        elif conteur < conteur_indice:
            if classe == "Pronoms personnels":
                groupe.append(mot)
            elif classe == "Verbes":
                groupe.append(mot)
                return groupe
            else:
                pass
        conteur_indice += 1

def poids(classe, conteur, phrase):
    if classe == "Pronoms personnels":
        print(principale(conteur, phrase))
    else:
        print('groupe inconnu')

def get_liste_classe():
    liste = []
    for file in files:
        if file.endswith(".json"):
            liste.append(os.path.basename(file).split(".")[0])
    return liste

def get_liste_composes():
    liste = []
    for file in files:
        if file.endswith(".json"):
            with open(file, 'r', encoding='utf8') as f:
                file = json.load(f)
            for element in file:
                if '[…]' in element:
                    liste.append(element)
    return liste

def get_liste_ne():
    liste = []
    for element in get_liste_composes():
        composant = element.split(" ")[-1]
        liste.append(composant)
    return liste


if __name__ == '__main__':
    for file in files:
        if file.endswith(".json"):
            with open(file, 'r', encoding='utf8') as f:
                liste = set(json.load(f))
                liste = list(liste)
            with open(file, 'w', encoding='utf8') as f:
                json.dump(liste, f, ensure_ascii=False, indent=4)


